import { ACTYPS } from './types';

export const ctxmenuShow = ({ top, left, uid, edit, remove }) => {
  return {
    type: ACTYPS.CTXMENU.SHOW,
    payload: { top, left, uid, edit, remove }
  };
};

export const ctxmenuHide = () => {
  return {
    type: ACTYPS.CTXMENU.HIDE
  };
};
