import { ACTYPS } from './types';

export const modalLogoutSet = (bool) => {
  return {
    type: ACTYPS.MODAL.LOGOUTSET,
    payload: bool
  };
};

export const modalSettingsSet = (bool) => {
  return {
    type: ACTYPS.MODAL.SETTINGSSET,
    payload: bool
  };
};

export const modalHowtoSet = (bool) => {
  return {
    type: ACTYPS.MODAL.HOWTOSET,
    payload: bool
  };
};

export const modalHowtoTypeSet = (bool) => {
  return {
    type: ACTYPS.MODAL.HOWTOTYPE,
    payload: bool
  };
};

export const modalMenuSet = (bool) => {
  return {
    type: ACTYPS.MODAL.MENUSET,
    payload: bool
  };
};

export const modalTaskFormSet = (bool) => {
  return {
    type: ACTYPS.MODAL.TASKFORMSET,
    payload: bool
  };
};

export const modalTaskFormTypeSet = (type) => {
  return {
    type: ACTYPS.MODAL.TASKFORMTYPESET,
    payload: type
  };
};

export const modalCurtainSet = (bool) => {
  return {
    type: ACTYPS.MODAL.CURTAINSET,
    payload: bool
  };
};
