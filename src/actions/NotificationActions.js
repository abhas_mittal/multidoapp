import { ACTYPS } from './types';

export const notificationAdd = (notification) => {
  return {
    type: ACTYPS.NOTIFICATION.ADD,
    payload: notification
  };
};

export const notificationRemove = () => {
  return {
    type: ACTYPS.NOTIFICATION.REMOVE
  };
};
