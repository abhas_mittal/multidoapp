import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, Animated, Easing } from 'react-native';
import { SortableContainer, SortableElement, arrayMove } from 'react-sortable-hoc';
import FlipMove from 'react-flip-move';
import { todoChange, todoSort, dateJump, ctxmenuHide, todoMove } from '../../actions';
import { Def, Txt, Func, Areas } from '../../config';
import TaskRow from './TaskRow';
import DayHeader from './DayHeader';
import { Tracker, Gac } from '../../ga';

const SortableItem = SortableElement(({ value }) => {
  return (
    <li
      key={`sort-li-${value.uid}`}
      data-uid={value.uid}
      style={{ paddingBottom: 10 }}
      className="taskItem"
    >
      <TaskRow task={value} />
    </li>
  );
});

const SortableList = SortableContainer(({ sorting, items, height = 300 }) => {
    return (
      <FlipMove
        duration={500} easing="ease-in-out" enterAnimation="fade" leaveAnimation="fade"
        disableAllAnimations={sorting}
        className="SortableList"
        style={{ paddingLeft: 20, paddingRight: 0, overflow: 'auto', height }}
        typeName="ul"
      >
        {items.map((value, index) =>
          <SortableItem key={`item-${value.uid}`} index={index} value={value} />
        )}
      </FlipMove>
    );
});

class DayPanel extends Component {

  state = {
    opacity: new Animated.Value(1),
    isAvailable: false,
    windowSize: null,
    scrollHeight: 0,
    sorting: false,
    emptyOpacity: new Animated.Value(0),
    emptyMargin: new Animated.Value(-100),
    emptyDuration: 500,
    dragarea: null,
    dropareas: null,
    dropdate: null,
    dropuid: null
  };

  componentWillMount() {
    this.createDataSource(this.props);
    this.setSizes(this.props);
    const { available } = this.props.date;
    if (available === this.props.day) {
      Animated.timing(this.state.opacity, { toValue: 0, easing: Easing.inOut(Easing.cubic), duration: Def.listDuration, }).start(() => {
        this.setState({ isAvailable: true });
        this.emptyAnimIn();
      });
    } else {
      this.setState({ isAvailable: false });
      Animated.timing(this.state.opacity, { toValue: 0.8, easing: Easing.inOut(Easing.cubic), duration: Def.listDuration, }).start(() => {
        this.emptyAnimOut();
      });
    }
  }

  componentDidMount() {
    if (this.state.scrollHeight === 0) {
      this.setSizes(this.props);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!Func.objEquivalent(this.props.rawtasks, nextProps.rawtasks)) { return true; }
    if (this.props.windowSize !== nextProps.windowSize) { return true; }
    if (this.state.scrollHeight !== nextState.scrollHeight) { return true; }
    if (this.props.date.available !== nextProps.date.available) {
      if (nextProps.date.available === this.props.day) { return true; }
      else if (this.props.date.available === this.props.day) { return true; }
    }
    if (this.state.isAvailable !== nextState.isAvailable) { return true; }
    if (this.state.sorting !== nextState.sorting) { return true; }
    if (this.state.dropareas !== nextState.dropareas) { return true; }
    if (this.state.dropdate !== nextState.dropdate) { return true; }
    if (this.state.dragarea !== nextState.dragarea) { return true; }
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (!Func.objEquivalent(this.props.rawtasks, nextProps.rawtasks)) {
      this.createDataSource(nextProps);
    }

    if (this.props.windowSize !== nextProps.windowSize) {
      this.setSizes(nextProps);
    }

    if (this.props.date.available !== nextProps.date.available) {
      if (nextProps.date.available === this.props.day) {
        Animated.timing(this.state.opacity, { toValue: 0, easing: Easing.inOut(Easing.cubic), duration: Def.listDuration, }).start(() => {
          this.setState({ isAvailable: true });
          this.emptyAnimIn();
        });
      } else if (this.props.date.available === this.props.day) {
        this.setState({ isAvailable: false });
        Animated.timing(this.state.opacity, { toValue: 0.8, easing: Easing.inOut(Easing.cubic), duration: Def.listDuration, }).start(() => {
          this.emptyAnimOut();
        });
      }
    }
  }

  emptyAnimIn() {
    Animated.parallel([
      Animated.timing(this.state.emptyOpacity, { toValue: 1, duration: this.state.emptyDuration }),
      Animated.timing(this.state.emptyMargin, { toValue: 0, duration: this.state.emptyDuration }),
    ]).start();
  }

  emptyAnimOut() {
    Animated.timing(this.state.emptyOpacity, { toValue: 0, duration: this.state.emptyDuration * 0.6 }).start(() => {
      this.state.emptyMargin.setValue(-100);
    });
  }

  setSizes({ windowSize }) {
    if (this.refs.DayPanelScroll !== undefined) {
      const scrollHeight = this.refs.DayPanelScroll._reactInternalInstance._renderedComponent._hostNode.clientHeight;
      this.setState({ windowSize, scrollHeight });
    }
  }

  createDataSource({ rawtasks }) {
    let tasks = {};
    if (typeof rawtasks === 'object' && rawtasks != null) {
      const alltasks = Object.keys(rawtasks).map(tsks => { return Object.assign({}, rawtasks[tsks], { uid: tsks, date: this.props.day }); });
      const doneTasks = alltasks.filter(t => t.done).sort((a, b) => { return b.pos - a.pos; });
      const undoneTasks = alltasks.filter(t => !t.done).sort((a, b) => { return b.pos - a.pos; });
      tasks = undoneTasks; tasks.push(...doneTasks);
    }

    this.dataSource = tasks;
  }

  dropTask() {
    this.props.todoMove({
      uid: this.state.dropuid,
      olddate: this.props.day,
      newdate: this.state.dropdate,
      data: this.props.rawtasks[this.state.dropuid],
    });

    this.setState({ dropdate: null, dropuid: null });
  }

  onSortEnd({ oldIndex, newIndex }) {
    Areas.remove();
    if (this.state.dropuid !== null && this.state.dropdate !== null && this.state.dropdate !== this.props.day) {
      this.refs.SortableList.cancel();
      this.setState({ sorting: false, dropareas: null, dragarea: null });
      this.dropTask();
      Tracker.userAction(Gac.usract.rowdroptodate);
    } else {
      this.setState({ dropareas: null, dragarea: null });
      const newDataSource = arrayMove(this.dataSource, oldIndex, newIndex);

      const newObject = {};
      newDataSource.forEach((elem, key) => {
        const position = (this.dataSource.length - 1) - key;
        newObject[elem.uid] = { text: elem.text, done: elem.done, pos: position };
      });

      this.props.todoSort(newObject, this.props.day, () => {
        this.setState({ sorting: false });
      });
      Tracker.userAction(Gac.usract.rowdroptosort);
    }
  }

  onSortStart(e) {
    this.setState({ sorting: true, dropareas: Areas.areas(), dragarea: Areas.current(), dropuid: e.node.attributes['data-uid'].nodeValue });
    this.props.ctxmenuHide();
    Tracker.userAction(Gac.usract.rowdrag);
  }

  onSortMove(e) {
    const dropto = Areas.dropto(e.clientX, e.clientY, this.state.dropareas);
    Areas.dragfrom(e.clientX, e.clientY, this.state.dragarea);
    this.setState({ dropdate: dropto });
  }

  moveToHere() {
    this.props.dateJump(this.props.day);
    Tracker.userAction(Gac.usract.daymovehere);
  }

  renderEmpty() {
    const opacity = this.state.emptyOpacity;
    const marginBottom = this.state.emptyMargin;
    return (
      <Animated.View style={[styles.emptyView, styles.emptyViewWide, { opacity, marginBottom }]}>
        <Text style={[styles.emptyTitle, styles.emptyTitleWide]}>{Txt.daypanel.listempty.Break()}</Text>
        <Text style={[styles.emptyText, styles.emptyTextWide]}>{Txt.daypanel.addhere}</Text>
      </Animated.View>
    );
  }

  renderListView() {
    const order = Object.keys(this.dataSource);
    if (!order.length) { return this.renderEmpty(); }
    return (
        <SortableList
          ref="SortableList"
          sorting={this.state.sorting}
          items={this.dataSource}
          onSortEnd={this.onSortEnd.bind(this)}
          onSortStart={this.onSortStart.bind(this)}
          onSortMove={this.onSortMove.bind(this)}
          useDragHandle
          pressDelay={0}
          height={this.state.scrollHeight}
        />
    );
  }

  renderCurrent() {
    let width = { width: Def.wideDaysWidth };
    if (!this.props.env.wide) {
      width = { width: this.props.env.width };
    }
    return [styles.day, width];
  }

  renderOverlayStyle() {
    const opacity = this.state.opacity;
    const animStyle = { opacity };
    if (this.state.isAvailable) {
      return null;
    }
    return [styles.overlay, animStyle];
  }

  renderDay() {
    return (
      <View style={this.renderCurrent()}>
        <Animated.View style={this.renderOverlayStyle()} data-type="list" data-dropdate={this.props.day}><TouchableOpacity onPress={this.moveToHere.bind(this)} style={styles.overlayButton} /></Animated.View>
        <View style={styles.top}>
          <DayHeader date={this.props.day} />
        </View>

        <View style={styles.content} className="DayPanelScroll" ref="DayPanelScroll" data-current={this.state.isAvailable}>
          {this.renderListView()}
        </View>

      </View>
    );
  }

  render() {
    return this.renderDay();
  }
}

const styles = {
  overlay: {
    position: 'absolute',
    zIndex: 200,
    opacity: 1,
    left: 10,
    right: 10,
    top: 0,
    bottom: 0,
    backgroundColor: Def.colorBlue
  },
  overlayButton: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  scrollview: {
    flex: 1,
    flexDirection: 'column',
  },
  customscrollview: {
    flex: 1,
    backgroundColor: 'rgba(0,255,0,0.3)'
  },
  day: {
    opacity: 1,
  },
  top: {
    paddingLeft: 20,
    paddingRight: 20
  },
  content: {
    flex: 1,
  },
  current: {
    opacity: 1,
  },
  emptyView: {
    flex: 1,
    opacity: 0,
    paddingRight: 20,
    paddingLeft: 20,
    paddingBottom: 20,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  emptyViewWide: {
    alignItems: 'center',
  },
  emptyTitle: {
    textAlign: 'right',
    fontFamily: Def.fontExtLight,
    color: 'rgba(255,255,255,0.4)',
    fontSize: 48,
    lineHeight: 44,
  },
  emptyTitleWide: {
    textAlign: 'center',
  },
  emptyText: {
    paddingTop: 10,
    opacity: 1,
    textAlign: 'right',
    fontFamily: Def.fontMedium,
    color: Def.colorWhite,
    fontSize: 16,
  },
  emptyTextWide: {
    textAlign: 'center',
  }
};


const mapStateToProps = (state, ownProps) => {
  const rawtasks = state.todo.datas ? state.todo.datas[ownProps.day] : {};
  return { ...state, rawtasks };
};

export default connect(mapStateToProps, { todoChange, todoSort, dateJump, ctxmenuHide, todoMove })(DayPanel);
