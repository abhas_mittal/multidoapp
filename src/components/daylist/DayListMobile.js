import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Animated, Easing } from 'react-native';
import { dateSetCurrent, dateSetAvailable, dateSetAvailableToCurrent, dateJump, dateJumpEnd, dateSetAnim, taskSetSortable } from '../../actions';
import { Def, Func } from '../../config';
import DayPanel from './DayPanel';
import Footer from './Footer';
import { Tracker, Gac } from '../../ga';

class DayListMobile extends Component {

  state = {
    translateX: new Animated.Value(0),
    opacity: new Animated.Value(1),
    clickable: true,
    jumping: null
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.date.current !== nextProps.date.current) { return true; }
    if (this.props.date.jump !== nextProps.date.jump) { return true; }
    if (this.state.jumping !== nextState.jumping) { return true; }
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    const { jump, current } = nextProps.date;
    if (this.props.date.jump !== jump) {
      if (Func.getDayDiffAbs(current, jump) > 1) {
        if (Func.getDayDiff(current, jump) < 0) { this.jumpToDate(jump, 'prev'); } else { this.jumpToDate(jump, 'next'); }
      } else {
        if (Func.getDayDiff(current, jump) < 0) { this.pressPrev(); } else { this.pressNext(); }
      }
    }

    if (this.state.jumping !== nextState.jumping) {
      if (nextState.jumping !== null) { this.jumpEnd(nextState.jumping); }
    }
  }

  jumpToDate(date, dir) {
    if (this.state.clickable) {
      this.setState({ clickable: false });
      this.props.dateSetAnim(true);
      const moveto = (dir === 'prev') ? Def.wideDaysWidth : -Def.wideDaysWidth;
      Animated.parallel([
        Animated.timing(this.state.opacity, { toValue: 0, duration: Def.jumpDuration, }),
        Animated.timing(this.state.translateX, { toValue: moveto, duration: Def.jumpDuration, }),
      ]).start(() => {
        this.props.dateSetAvailable(date);
        this.props.dateSetAvailableToCurrent();
        this.setState({ jumping: dir });
      });
    }
  }

  jumpEnd(jumping) {
    const moveto = (jumping === 'prev') ? -Def.wideDaysWidth : Def.wideDaysWidth;
    const backduration = this.props.env.isandroid ? 400 : 0;

    Animated.sequence([
      Animated.timing(this.state.translateX, { toValue: moveto, duration: backduration, }),
      Animated.parallel([
        Animated.timing(this.state.opacity, { toValue: 1, duration: Def.jumpDuration, }),
        Animated.timing(this.state.translateX, { toValue: 0, duration: Def.jumpDuration, }),
      ])
    ]).start(() => {
      this.props.dateJumpEnd();
      this.props.dateSetAnim(false);
      this.setState({ jumping: null, clickable: true });
    });
  }

  pressMenu() {
    this.props.showMenu();
  }

  pressToday() {
    if (this.props.date.today !== this.props.date.current) { this.props.dateJump(this.props.date.today); }
    Tracker.userAction(Gac.usract.foottoday);
  }

  pressPrev() {
    let step = this.props.env.width;
    if (this.props.env.wide) { step = Def.wideDaysWidth; }

    if (this.state.clickable) {
      this.setState({ clickable: false });
      this.props.dateSetAnim(true);
      this.props.dateSetAvailable(Func.getDay(-1, this.props.date.current));
      Animated.timing(this.state.translateX, { toValue: step, easing: Easing.inOut(Easing.cubic), duration: Def.listDuration, }).start(() => {
        this.props.dateSetAvailableToCurrent();
        this.props.dateJumpEnd();
        this.props.dateSetAnim(false);
        this.state.translateX.setValue(0);
        this.setState({ clickable: true });
      });
      Tracker.userAction(Gac.usract.footprev);
    }
  }

  pressNext() {
    let step = this.props.env.width;
    if (this.props.env.wide) { step = Def.wideDaysWidth; }

    if (this.state.clickable) {
      this.setState({ clickable: false });
      this.props.dateSetAnim(true);
      this.props.dateSetAvailable(Func.getDay(1, this.props.date.current));
      Animated.timing(this.state.translateX, { toValue: -step, easing: Easing.inOut(Easing.cubic), duration: Def.listDuration, }).start(() => {
        this.props.dateSetAvailableToCurrent();
        this.props.dateJumpEnd();
        this.props.dateSetAnim(false);
        this.state.translateX.setValue(0);
        this.setState({ clickable: true });
      });
      Tracker.userAction(Gac.usract.footnext);
    }
  }

  generateDays() {
    const { current } = this.props.date;
    let pnum = 1;
    if (this.props.env.wide) { pnum = 2; }
    if (this.props.env.isdesktop) { pnum = 4; }
    const dates = [];

    for (let i = -pnum; i < (pnum + 1); i++) {
      const d = Func.getDay(i, current);
      dates.push(d);
    }

    return dates.map((date) =>
      <DayPanel day={date} key={date} />
    );
  }

  renderDays() {
    const { translateX, opacity } = this.state;
    const animStyle = { transform: [{ translateX }] };

    return (
      <View style={styles.container}>
        <View style={styles.days}>
          <Animated.View style={[styles.animated, animStyle, { opacity }]}>
            {this.generateDays()}
          </Animated.View>
        </View>
        <Footer pressNext={this.pressNext.bind(this)} pressPrev={this.pressPrev.bind(this)} pressToday={this.pressToday.bind(this)} />
      </View>
    );
  }

  render() {
    return this.renderDays();
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  days: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'stretch',
    paddingTop: Def.paddingTop,
  },
  daycont: {
    flex: 1,
    flexDirection: 'row',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  animated: {
    width: 3000,
    flexDirection: 'row',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'stretch',
  }
};

const mapStateToProps = ({ env, date }) => {
  return { env, date };
};

export default connect(mapStateToProps, {
  dateSetCurrent, dateSetAvailable, dateSetAvailableToCurrent, dateJump, dateJumpEnd, dateSetAnim, taskSetSortable
})(DayListMobile);
