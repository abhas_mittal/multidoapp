import React, { Component } from 'react';
import FooterMobile from './FooterMobile';

class Footer extends Component {
  render() {
    return <FooterMobile {...this.props} />;
  }
}

export default Footer;
