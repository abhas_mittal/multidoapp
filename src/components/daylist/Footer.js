import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { IconButton, InputAdd } from '../common';
import { todoAdd } from '../../actions';
import { Def, Func } from '../../config';
import { Tracker, Gac } from '../../ga';

class Footer extends Component {
  state = {
    inputtext: '',
    lastpos: 0
  };

  componentWillMount() {
    this.setLastpos(this.props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.date.current !== this.props.date.current) return true;
    if (nextState.inputtext !== this.state.inputtext) return true;

    if (nextProps.todo.settings !== null) {
      if (this.props.todo.settings.autoCorrection !== nextProps.todo.settings.autoCorrection) return true;
    }

    if (Func.datasAvailable(this.props.todo.datas, nextProps.todo.datas)) {
      if (!Func.objEquivalent(this.props.todo.datas[this.props.date.current], nextProps.todo.datas[this.props.date.current])) {
        return true;
      }
    }

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.date.current !== this.props.date.current) {
      this.setLastpos(nextProps);
    }

    if (Func.datasAvailable(this.props.todo.datas, nextProps.todo.datas)) {
      if (!Func.objEquivalent(this.props.todo.datas[this.props.date.current], nextProps.todo.datas[this.props.date.current])) {
        this.setLastpos(nextProps);
      }
    }
  }

  setLastpos({ todo, date }) {
    if (Func.dayAvailable(todo.datas, date.current)) {
      const tasks = Object.keys(todo.datas[date.current]).map(tsks => { return Object.assign({}, todo.datas[date.current][tsks], { uid: tsks }); });
      const maxpos = ((tasks.length > 0) ? Math.max.apply(Math, tasks.map((o) => { return o.pos; })) : 0);
      this.setState({ lastpos: maxpos });
    }
  }

  addTask() {
    if (this.state.inputtext.length > 1) {
      Tracker.userAction(Gac.usract.footfieldadd);
      this.props.todoAdd({ text: this.state.inputtext, done: false, date: this.props.date.current, pos: (this.state.lastpos + 1) });
      this.setState({ inputtext: '' });
      setTimeout(() => { this.refs.InputAdd.refs.input.focus(); }, 100);
    }
  }

  render() {
    return (
      <View style={styles.view}>
        <View style={styles.left}>
          <IconButton icon="left" onPress={this.props.pressPrev} ontouch />
        </View>

        <View style={styles.center}>
          <InputAdd
            ref="InputAdd"
            value={this.state.inputtext}
            onChangeText={inputtext => this.setState({ inputtext })}
            addTask={this.addTask.bind(this)}
            autoCorrect={this.props.todo.settings.autoCorrection}
          />
        </View>

        <View style={styles.right}>
          <IconButton icon="right" onPress={this.props.pressNext} ontouch />
        </View>
      </View>
    );
  }
}

const styles = {
  view: {
    flexDirection: 'row',
    alignItems: 'stretch',
    height: 50,
    backgroundColor: Def.colorGray
  },
  left: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 10,
  },
  right: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 10,
  },
  center: {
    width: 360,
    backgroundColor: Def.colorWhite
  }
};


const mapStateToProps = ({ env, date, task, todo }) => {
  return { env, date, task, todo };
};

export default connect(mapStateToProps, { todoAdd })(Footer);
