import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { SortableHandle } from 'react-sortable-hoc';
import { ctxmenuShow, ctxmenuHide, todoDelete, todoChange } from '../../actions';
import Icon from '../common/Icons';
import { Def } from '../../config';
import { Tracker, Gac } from '../../ga';

const DragHandle = SortableHandle((props) => <TouchableOpacity {...props} />);

class TaskRow extends Component {

  state = {
    lastPress: 0,
    edit: false,
    inputtext: null,
    ctxMenuVisible: true,
    ctxPos: { top: 0, left: 0 }
  };


  componentWillMount() {
    this.setState({ inputtext: this.props.task.text });
  }

  componentDidUpdate() {
    if (this.state.edit) {
      const refid = `TaskRowEditInput-${this.props.task.uid}`;
      setTimeout(() => { this.refs[refid].focus(); }, 100);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.edit !== nextState.edit) return true;
    if (nextState.inputtext !== this.state.inputtext) return true;
    if (this.props.task.text !== nextProps.task.text) return true;
    if (this.props.task.done !== nextProps.task.done) return true;
    if (this.props.task.pos !== nextProps.task.pos) return true;

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.task.text !== nextProps.task.text) {
      this.setState({ inputtext: nextProps.task.text });
    }
  }

  onPress(e) {
    const delta = new Date().getTime() - this.state.lastPress;
    if (delta < 200) { this.doublePress(e); }
    this.setState({ lastPress: new Date().getTime() });
  }

  doublePress(e) {
    this.setState({ edit: true });
    Tracker.userAction(Gac.usract.rowdoublepress);
  }

  setEditByCtx() {
    this.setState({ edit: true });
    this.props.ctxmenuHide();
  }

  removeByCtx() {
    this.props.todoDelete({ uid: this.props.task.uid, date: this.props.task.date });
    this.props.ctxmenuHide();
  }

  taskDone() {
    const { date, done, pos, text, uid } = this.props.task;
    this.props.todoChange({ text, done: !done, date, pos, uid });
    Tracker.userAction(Gac.usract.rowdone);
  }

  editCancel() {
    this.setState({ inputtext: this.props.task.text });
    this.setState({ edit: false });
    Tracker.userAction(Gac.usract.roweditcancel);
  }

  onSubmitEditing() {
    if (this.state.inputtext.length > 1) {
      const { date, done, pos, uid } = this.props.task;
      this.props.todoChange({ text: this.state.inputtext, done, date, pos, uid });
      this.setState({ edit: false });
    }
  }

  rightClick(e) {
    e.preventDefault();
    this.props.ctxmenuShow({ top: e.clientY, left: e.clientX, uid: this.props.task.uid, edit: this.setEditByCtx.bind(this), remove: this.removeByCtx.bind(this) });
    Tracker.userAction(Gac.usract.rowctx);
  }

  renderDragHandle() {
    if (this.props.ctxmenu.visible) { return null; }

    return (
      <View style={styles.dragHandleView}>
        <DragHandle activeOpacity={1} onPress={this.onPress.bind(this)} style={styles.dragHandle} className="rowTouch" />
      </View>
    );
  }

  renderViewStyle() {
    if (this.props.task.done) {
      return [styles.view, styles.viewdone];
    }
    return styles.view;
  }

  renderView() {
    const { uid, text, done } = this.props.task;
    const refid = `TaskRowEditInput-${uid}`;

    if (this.state.edit) {
      return (
        <View style={this.renderViewStyle()}>

          <View style={styles.inputView}>
            <TextInput
              ref={refid}
              id={refid}
              style={styles.input}
              value={this.state.inputtext}
              onChangeText={inputtext => this.setState({ inputtext })}
              className="inputedit"
              onSubmitEditing={this.onSubmitEditing.bind(this)}
              autoFocus
            />
          </View>

          <View style={styles.cancelCont} className="pipeTouch">
            <TouchableOpacity activeOpacity={0.6} style={styles.cancelButton} onPress={this.editCancel.bind(this)}>
              <Icon src="close" fill={Def.colorBlue} />
            </TouchableOpacity>
          </View>

        </View>
      );
    }

    return (
      <View style={this.renderViewStyle()} onContextMenu={this.rightClick.bind(this)}>
        {this.renderDragHandle()}

        <View style={styles.textView}>
          <Text style={styles.text} numberOfLines={1}>{text}</Text>
        </View>

        <View style={styles.pipeCont} className="pipeTouch">
          <TouchableOpacity activeOpacity={0.6} style={styles.pipeButton} onPress={this.taskDone.bind(this)}>
            <Icon src="pipe" fill={done ? Def.colorWhite : Def.colorIcon} />
          </TouchableOpacity>
        </View>

      </View>
    );
  }

  render() {
    return this.renderView();
  }
}

const styles = {
  view: {
    height: Def.taskHeight,
    width: 360,
    backgroundColor: Def.colorWhite,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  viewdone: {
    backgroundColor: 'rgba(255,255,255,0.2)',
    shadowOpacity: 0,
  },
  rightclickHandle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  dragHandleView: {
    position: 'absolute',
    zIndex: 100,
    left: 0,
    right: Def.taskHeight,
    top: 0,
    bottom: 0,
  },
  dragHandle: {
    flex: 1,
  },
  textView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: Def.taskPaddingLeft,
    paddingRight: Def.taskPaddingRight,
  },
  text: {
    fontFamily: Def.fontMedium,
    color: Def.colorBlack,
    fontSize: Def.taskTextSize,
    alignSelf: 'center',
  },
  inputView: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: Def.colorGray,
  },
  input: {
    flex: 1,
    fontFamily: Def.fontMedium,
    color: Def.colorBlack,
    fontSize: Def.taskTextSize,
    paddingLeft: Def.taskPaddingLeft,
    paddingRight: Def.taskPaddingRight,
  },
  pipeCont: {
    opacity: 1,
    width: Def.taskHeight,
    overflow: 'hidden',
  },
  pipeButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelCont: {
    opacity: 1,
    width: Def.taskHeight,
    overflow: 'hidden',
    borderLeftColor: Def.colorWhite,
    borderLeftWidth: 1,
    backgroundColor: Def.colorGray
  },
  cancelButton: {
    flex: 1,
    paddingTop: 2,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

const mapStateToProps = ({ ctxmenu }) => {
  return { ctxmenu };
};

export default connect(mapStateToProps, { ctxmenuShow, ctxmenuHide, todoDelete, todoChange })(TaskRow);
