import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { Def } from '../../config';

const Loader = ({ color = '#FFFFFF' }) => {
  let newcolor = '#FFFFFF';
  if (color === 'blue') { newcolor = Def.colorBlue; }

  return (
    <View style={styles.container}>
      <ActivityIndicator size='small' color={newcolor} />
    </View>
  );
};

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
};


export { Loader };
