import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { Def } from '../../config';

const ButtonBlue = ({ onPress, children }) => {
  return (
    <View style={styles.view} className="buttonCenter">
      <TouchableHighlight underlayColor={Def.colorInfo} onPress={onPress} style={styles.button}>
        <Text style={styles.text}>{children}</Text>
      </TouchableHighlight>
    </View>
  );
};

const styles = {
  text: {
    fontFamily: Def.fontMedium,
    alignSelf: 'center',
    color: Def.colorWhite,
    fontSize: 16,
    paddingTop: 15,
    paddingBottom: 13
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Def.colorBlue,
    borderRadius: 0,
    borderWidth: 0
  },
  view: {
    flex: 1,
    height: Def.buttonHeight,
  }
};

export { ButtonBlue };
