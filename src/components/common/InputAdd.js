import React, { Component } from 'react';
import { TextInput, View } from 'react-native';
import { IconButton } from './';
import { Def, Txt } from '../../config';

class InputAdd extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.value !== nextProps.value) return true;
    if (this.props.autoCorrect !== nextProps.autoCorrect) return true;
    return false;
  }

  render() {
    return (
        <View style={styles.view}>
          <TextInput
            ref="input"
            placeholder={Txt.taskform.addtaskplaceholder}
            style={styles.input}
            value={this.props.value}
            onChangeText={this.props.onChangeText}
            className="inputadd"
            onSubmitEditing={this.props.addTask}
            autoCorrect={this.props.autoCorrect}
            autoCapitalize="sentences"
          />
          <IconButton icon="plus" onPress={this.props.addTask} ontouch reverse />
        </View>

    );
  }
}

const styles = {
  view: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  input: {
    flex: 1,
    fontFamily: Def.fontMedium,
    color: Def.colorBlack,
    fontSize: 14,
    lineHeight: 30,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'left',
    textAlignVertical: 'bottom',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 4,
    paddingBottom: 4,
  }
};

export { InputAdd };
