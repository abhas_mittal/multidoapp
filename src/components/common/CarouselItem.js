import React, { Component } from 'react';
import { View, Text, Animated, Easing, Platform } from 'react-native';
import { Link } from './';
import { Def, Img, Txt } from '../../config';

const imgshift = 400;
const txtshift = 200;

class CarouselItem extends Component {

  state = {
    imageOpac: new Animated.Value(0),
    imageScale: new Animated.Value(0),
    imageX: new Animated.Value(imgshift),
    titleOpac: new Animated.Value(0),
    titleX: new Animated.Value(txtshift),
    leadOpac: new Animated.Value(0),
    leadX: new Animated.Value(txtshift),
    animDuration: 400,
    animBounce: 0.8,
    animDelay: 100,
  };

  componentWillMount() {
    this.moveIn('left');
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.nextItem !== nextProps.nextItem) return true;
    if (this.props.currentItem !== nextProps.currentItem) return true;
    if (this.props.data.img !== nextProps.data.img) return true;
    if (this.props.animation !== nextProps.animation) return true;
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.nextItem !== nextProps.nextItem) {
      if (this.props.nextItem === (this.props.length - 1) && nextProps.nextItem === 0) {
        this.moveOut('left');
      } else if (this.props.nextItem === 0 && nextProps.nextItem === (this.props.length - 1)) {
        this.moveOut('right');
      } else if (nextProps.nextItem > this.props.nextItem) {
        this.moveOut('left');
      } else {
        this.moveOut('right');
      }
    }

    if (this.props.currentItem !== nextProps.currentItem) {
      if (this.props.currentItem === (this.props.length - 1) && nextProps.currentItem === 0) {
        this.moveIn('left');
      } else if (this.props.currentItem === 0 && nextProps.currentItem === (this.props.length - 1)) {
        this.moveIn('right');
      } else if (nextProps.currentItem > this.props.currentItem) {
        this.moveIn('left');
      } else {
        this.moveIn('right');
      }
    }
  }

  moveOut(dir) {
    Animated.sequence([
      Animated.stagger(this.state.animDelay, [
        Animated.parallel([
          Animated.timing(this.state.imageOpac, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
          Animated.timing(this.state.imageScale, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
          Animated.timing(this.state.imageX, { toValue: (dir === 'right') ? imgshift : -imgshift, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
        ]),
        Animated.parallel([
          Animated.timing(this.state.titleOpac, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
          Animated.timing(this.state.titleX, { toValue: (dir === 'right') ? txtshift : -txtshift, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
        ]),
        Animated.parallel([
          Animated.timing(this.state.leadOpac, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
          Animated.timing(this.state.leadX, { toValue: (dir === 'right') ? txtshift : -txtshift, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
        ]),
      ]),
    ]).start(() => {
      this.props.onAnimationOutEnd();
    });
  }

  moveIn(dir) {
    if (this.props.animation) {
      this.state.imageX.setValue((dir === 'left') ? imgshift : -imgshift);
      this.state.titleX.setValue((dir === 'left') ? txtshift : -txtshift);
      this.state.leadX.setValue((dir === 'left') ? txtshift : -txtshift);

      Animated.sequence([
        Animated.stagger(this.state.animDelay, [
          Animated.parallel([
            Animated.timing(this.state.imageOpac, { toValue: 1, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
            Animated.timing(this.state.imageScale, { toValue: 1, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
            Animated.timing(this.state.imageX, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
          ]),
          Animated.parallel([
            Animated.timing(this.state.titleOpac, { toValue: 1, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
            Animated.timing(this.state.titleX, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
          ]),
          Animated.parallel([
            Animated.timing(this.state.leadOpac, { toValue: 1, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
            Animated.timing(this.state.leadX, { toValue: 0, easing: Easing.out(Easing.back(this.state.animBounce)), duration: this.state.animDuration }),
          ]),
        ]),
      ]).start(() => {
        this.props.onAnimationInEnd();
      });
    } else {
      this.state.imageOpac.setValue(1);
      this.state.imageScale.setValue(1);
      this.state.imageX.setValue(0);
      this.state.titleOpac.setValue(1);
      this.state.titleX.setValue(0);
      this.state.leadOpac.setValue(1);
      this.state.leadX.setValue(0);
      this.props.onAnimationInEnd();
    }
  }

  renderImage() {
    return Img[this.props.data.img];
  }

  renderLink() {
    if (this.props.currentItem === 0) {
      return <Link url={Txt.howto.url} style={styles.linktext}>{Txt.howto.link}</Link>;
    }
    return null;
  }

  render() {
    return (
      <View style={styles.container}>

        <Animated.View style={[styles.imagecont, { opacity: this.state.imageOpac, transform: [{ translateX: this.state.imageX }, { scale: this.state.imageScale }] }]}>
          {this.renderImage()}
        </Animated.View>

        <Animated.View style={[styles.titlecont, { opacity: this.state.titleOpac, transform: [{ translateX: this.state.titleX }] }]}>
          <Text style={styles.title}>{this.props.data.title}</Text>
        </Animated.View>

        <Animated.View style={[styles.leadcont, { opacity: this.state.leadOpac, transform: [{ translateX: this.state.leadX }] }]}>
          <Text style={styles.lead}>{this.props.data.lead} {this.renderLink()}</Text>

        </Animated.View>

      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
  },
  imagecont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titlecont: {
    marginTop: (Platform.OS === 'web') ? 40 : 30,
  },
  title: {
    fontFamily: Def.fontLight,
    color: Def.colorBlue,
    fontSize: 32,
  },
  leadcont: {
    marginTop: 10,
  },
  lead: {
    fontFamily: Def.fontLight,
    color: Def.colorBlack,
    fontSize: 16,
  },
  linktext: {
    fontFamily: Def.fontMedium,
    color: Def.colorBlue,
    fontSize: 16,
    textDecorationLine: 'underline',
  }

};

export { CarouselItem };
