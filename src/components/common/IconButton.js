import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Def } from '../../config';
import Icon from './Icons';

const IconButton = (props) => {
  let color = Def.colorIcon;
  if (props.reverse) { color = Def.colorBlue; }
  if (props.color) { color = props.color; }

  return (
    <View style={styles.view}>
      <TouchableOpacity onPress={props.onPress} activeOpacity={0.6} style={styles.button} >
        <Icon src={props.icon} fill={color} today={props.today} />
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 0,
    borderWidth: 0
  },
  view: {
    width: 50,
    height: 50,
  }
};

export { IconButton };
