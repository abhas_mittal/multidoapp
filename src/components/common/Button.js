import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { Def } from '../../config';

const Button = ({ onPress, children }) => {
  return (
    <View style={styles.view} className="buttonCenter">
      <TouchableHighlight underlayColor="#F2F2F2" onPress={onPress} style={styles.button}>
        <Text style={styles.text}>{children}</Text>
      </TouchableHighlight>
    </View>
  );
};

const styles = {
  text: {
    fontFamily: Def.fontMedium,
    alignSelf: 'center',
    color: Def.colorBlue,
    fontSize: 16,
    paddingTop: 15,
    paddingBottom: 13
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Def.colorWhite,
    borderRadius: 0,
    borderWidth: 0
  },
  view: {
    flex: 1,
    height: Def.buttonHeight,
  }
};

export { Button };
