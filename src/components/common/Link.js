import React, { Component } from 'react';
import $ from 'jquery';
import { Linking, Text, Platform } from 'react-native';

class Link extends Component {

  shouldComponentUpdate(nextProps, nextState) {
    return false;
  }

  onPress() {
    if (Platform.OS === 'web') {
      if ($('#AppContainer').attr('data-apptype') === 'desktop') {
        multidoExternalLink(this.props.url); // Call electron external url by electronenv.js
      } else {
        Linking.openURL(this.props.url);
      }
    } else {
      Linking.canOpenURL(this.props.url).then(supported => {
        if (supported) { Linking.openURL(this.props.url); }
      });
    }
  }

  render() {
    return <Text onPress={this.onPress.bind(this)} style={this.props.style}>{this.props.children}</Text>;
  }
}


export { Link };
