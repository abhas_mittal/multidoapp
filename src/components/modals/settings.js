import React, { Component } from 'react';
import { Text, View, Animated, Easing } from 'react-native';
import { connect } from 'react-redux';
import { modalSettingsSet, todoSettings } from '../../actions';
import Modaler from './Modaler';
import { TextButtonBlue, Switch } from '../common';
import { Def, Txt } from '../../config';
import { Tracker, Gac } from '../../ga';

class Settings extends Component {

  state = {
    appeard: false,
    opacity: new Animated.Value(0),
    bottom: new Animated.Value(2000),
    animDuration: Def.modalDuration,
  };

  componentWillMount() {
    if (this.props.visible) {
      Tracker.screenView(Gac.scr.settings);
      Animated.timing(this.state.bottom, { toValue: 0, easing: Easing.out(Easing.back(0.8)), duration: this.state.animDuration }).start();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.visible !== nextProps.visible) return true;
    if (this.state.switchTest !== nextState.switchTest) return true;
    if (nextProps.todo.settings !== null) {
      if (this.props.todo.settings.firstdaySun !== nextProps.todo.settings.firstdaySun) return true;
      if (this.props.todo.settings.autoCorrection !== nextProps.todo.settings.autoCorrection) return true;
    }

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (!this.props.visible && nextProps.visible) {
      Tracker.screenView(Gac.scr.settings);
      Animated.timing(this.state.bottom, { toValue: 0, easing: Easing.out(Easing.back(0.8)), duration: this.state.animDuration }).start();
    }
  }

  onCancelPress() {
    Tracker.userAction(Gac.usract.setgoback);
    Animated.timing(this.state.bottom, { toValue: 2000, duration: this.state.animDuration }).start(() => {
      this.props.modalSettingsSet(false);
    });
  }

  changeFirstday(value) {
    Tracker.userAction(Gac.usract.setweekday);
    this.props.todoSettings({ firstdaySun: value, autoCorrection: this.props.todo.settings.autoCorrection });
  }

  changeAutocorrect(value) {
    Tracker.userAction(Gac.usract.setcorrect);
    this.props.todoSettings({ firstdaySun: this.props.todo.settings.firstdaySun, autoCorrection: value });
  }


  renderForm() {
    return (
      <View style={styles.container}>

        <View style={styles.row}>
          <View style={styles.label}>
            <Text style={styles.labeltext}>{Txt.settings.daylabel.Break()}</Text>
          </View>
          <View style={styles.switchrow}>
            <View style={styles.switchcont}>
              <Switch
                dataId="firstdaySwitch"
                trueLabel="Sunday"
                falseLabel="Monday"
                value={this.props.todo.settings.firstdaySun}
                onValueChange={this.changeFirstday.bind(this)}
              />
            </View>
          </View>
        </View>

        <View style={styles.separator} />

        <View style={styles.row}>
          <View style={styles.label}>
            <Text style={styles.labeltext}>{Txt.settings.correctlabel.Break()}</Text>
          </View>
          <View style={[styles.switchrow, styles.switchrowsub]}>
            <Text style={styles.sublabeltext}>{Txt.settings.correctsublabel}</Text>
            <View style={styles.switchcont}>
              <Switch
                dataId="autocorrectSwitch"
                trueLabel="Yes"
                falseLabel="No"
                value={this.props.todo.settings.autoCorrection}
                onValueChange={this.changeAutocorrect.bind(this)}
              />
            </View>
          </View>
        </View>

        <View style={styles.separator} />

        <View style={[styles.section, styles.foot]}>
          <TextButtonBlue onPress={this.onCancelPress.bind(this)}>{Txt.settings.cancel}</TextButtonBlue>
        </View>
      </View>
    );
  }

  renderWrapperStyle() {
    const bottom = this.state.bottom;
    return [styles.wrapper, { bottom }];
  }

  renderWrapper() {
    if (this.props.env.wide) {
      return (
        <Animated.View style={this.renderWrapperStyle()}>
          <View style={styles.contentcont}>
            <View style={styles.box}>
              {this.renderForm()}
            </View>
          </View>
        </Animated.View>
      );
    }

    return (
      <Animated.View style={this.renderWrapperStyle()}>
        <View style={styles.contentcont}>
          {this.renderForm()}
        </View>
      </Animated.View>
    );
  }

  render() {
    return (
      <Modaler {...this.props} onRequestClose={this.onCancelPress.bind(this)}>
        <View style={styles.inmodaler}>
          {this.renderWrapper()}
        </View>
      </Modaler>
    );
  }
}


const styles = {
  inmodaler: {
    flex: 1, // is important
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    position: 'absolute',
    zIndex: 0,
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    overflow: 'hidden',
    backgroundColor: Def.colorWhite
  },
  contentcont: {
    flex: 1,
    justifyContent: 'center',
  },
  box: {
    alignSelf: 'center',
    width: 400,
    height: 350,
  },
  container: {
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  section: {
    flexDirection: 'row',
    paddingBottom: 10
  },
  head: {
    paddingBottom: 30
  },
  foot: {
    paddingTop: 20
  },

  separator: {
    height: 1,
    marginTop: 30,
    marginBottom: 30,
    backgroundColor: 'rgba(0,0,0,0.05)'
  },

  switchrow: {
    marginTop: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  switchrowsub: {
    justifyContent: 'space-between',
  },
  switchcont: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  labeltext: {
    fontFamily: Def.fontLight,
    color: Def.colorBlack,
    fontSize: 22,
  },
  sublabeltext: {
    fontFamily: Def.fontLight,
    alignSelf: 'center',
    color: Def.colorTextGray,
    fontSize: 14,
  }

};


const mapStateToProps = ({ env, todo }) => {
  return { env, todo };
};

export default connect(mapStateToProps, { modalSettingsSet, todoSettings })(Settings);
