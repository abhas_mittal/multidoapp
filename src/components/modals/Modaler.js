import React, { Component } from 'react';
import { View } from 'react-native';

class Modaler extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.visible !== nextProps.visible) return true;
    if (this.props.firsttime !== nextProps.firsttime) return true;
    if (this.props.childrenUpdate) {
      if (this.props.children !== nextProps.children) return true;
    }
    return false;
  }

  renderStyle() {
    return this.props.visible ? styles.modalervisible : styles.modalerhidden;
  }

  render() {
    return (
      // {...this.props}
      <View style={this.renderStyle()}>
        {this.props.children}
      </View>
    );
  }
}

const styles = {
  modalervisible: {
    position: 'fixed',
    zIndex: 1000,
    opacity: 1,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  modalerhidden: {
    position: 'fixed',
    zIndex: -5000,
    opacity: 1,
    left: 0,
    right: 0,
    width: 0,
    height: 0,
    overflow: 'hidden',
  }
};

export default Modaler;
