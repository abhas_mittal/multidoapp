import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  text: null,
  type: null,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.NOTIFICATION.ADD:
        return action.payload;
      case ACTYPS.NOTIFICATION.REMOVE:
        return INITIAL_STATE;
      default:
        return state;
    }
};
