import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  email: '',
  emailError: '',
  password: '',
  passwordError: '',
  user: null,
  loggedin: null,
  error: '',
  loading: false,
  resetmailsended: null,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.AUTH.EMAILCHANGED:
        return { ...state, email: action.payload };
      case ACTYPS.AUTH.EMAILERROR:
        return { ...state, emailError: action.payload };
      case ACTYPS.AUTH.PASSWORDCHANGED:
        return { ...state, password: action.payload };
      case ACTYPS.AUTH.PASSWORDERROR:
        return { ...state, passwordError: action.payload };

      case ACTYPS.LOGIN.AUTOLOGIN:
        return { ...state, loggedin: action.payload };
      case ACTYPS.LOGIN.PROGRESS:
        return { ...state, loading: true, error: '' };
      case ACTYPS.LOGIN.SUCCESS:
        return { ...state, ...INITIAL_STATE, user: action.payload };
      case ACTYPS.LOGIN.FAIL:
        return { ...state, password: '', loading: false };

      case ACTYPS.REGISTRATION.AUTOLOGIN:
        return { ...state, loggedin: action.payload };
      case ACTYPS.REGISTRATION.PROGRESS:
        return { ...state, loading: true, error: '' };
      case ACTYPS.REGISTRATION.SUCCESS:
        return { ...state, ...INITIAL_STATE, user: action.payload };
      case ACTYPS.REGISTRATION.FAIL:
        return { ...state, password: '', loading: false };

      case ACTYPS.FORGOTTENPASS.PROGRESS:
        return { ...INITIAL_STATE, loading: true, error: '', loggedin: false };
      case ACTYPS.FORGOTTENPASS.SUCCESS:
        return { ...INITIAL_STATE, loggedin: false, resetmailsended: true };
      case ACTYPS.FORGOTTENPASS.FAIL:
        return { ...INITIAL_STATE, loggedin: false };
      case ACTYPS.FORGOTTENPASS.MAILSENDED:
        return { ...INITIAL_STATE, loggedin: false };

      case ACTYPS.AUTH.RESET:
        return { ...INITIAL_STATE, loggedin: false };
      default:
        return state;
    }
};
