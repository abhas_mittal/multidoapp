import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  platf: null,
  os: null,
  isdesktop: null,
  ismobile: null,
  isios: null,
  isandroid: null,
  width: null,
  height: null,
  wide: null,
  thin: null,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.ENV.SET:
        return { ...state,
          platf: action.payload.platf,
          os: action.payload.os,
          isdesktop: action.payload.isdesktop,
          ismobile: action.payload.ismobile,
          isios: action.payload.isios,
          isandroid: action.payload.isandroid,
          width: action.payload.width,
          height: action.payload.height,
          wide: action.payload.wide,
          thin: action.payload.thin,
        };
      case ACTYPS.ENV.KEYBOARD:
        return { ...state, keyboardopened: action.payload };
      default:
        return state;
    }
};
