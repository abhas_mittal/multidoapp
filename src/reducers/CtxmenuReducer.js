import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  visible: false,
  top: 0,
  left: 0,
  uid: null,
  edit: null,
  remove: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.CTXMENU.SHOW:
        return { visible: true, top: action.payload.top, left: action.payload.left, uid: action.payload.uid, edit: action.payload.edit, remove: action.payload.remove };
      case ACTYPS.CTXMENU.HIDE:
        return INITIAL_STATE;
      default:
        return state;
    }
};
