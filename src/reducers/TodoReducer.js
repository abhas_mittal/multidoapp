import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  started: false,
  received: false,
  datas: null,
  settings: null,
  visited: null,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.TODO.FETCH:
        return { ...state, started: action.payload };
      case ACTYPS.TODO.FETCHSUCCESS:
        if (action.payload === null) {
          return { ...state, received: true };
        }
        return {
          ...state,
          received: true,
          datas: action.payload.todos || null,
          settings: action.payload.settings || null,
          visited: action.payload.visited || null,
        };
      case ACTYPS.TODO.CHANGE:
        return state;
      case ACTYPS.TODO.MOVE:
        return state;
      case ACTYPS.TODO.DELETE:
        return state;
      case ACTYPS.TODO.SORT:
        return state;
      case ACTYPS.TODO.SETTINGS:
        return state;
        case ACTYPS.TODO.VISITED:
          return state;
      case ACTYPS.TODO.RESET:
        return { ...INITIAL_STATE, started: true };
      case ACTYPS.TODO.SETINIT:
        return INITIAL_STATE;
      default:
        return state;
    }
};
