import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  inputtitle: '',
  inputtitleError: '',
  inputdate: new Date(),
  taskdatas: null,
  sortablelist: false,
  sums: {}
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.TASK.DATECHANGED:
          return { ...state, inputdate: action.payload };
      case ACTYPS.TASK.TITLECHANGED:
          return { ...state, inputtitle: action.payload };
      case ACTYPS.TASK.TITLEERROR:
        return { ...state, inputtitleError: action.payload };
      case ACTYPS.TASK.SETDATE:
        return { ...state, inputdate: action.payload };
      case ACTYPS.TASK.SETSUM:
        const sums = { ...state.sums, [action.payload.date]: action.payload };
        return { ...state, sums };
      case ACTYPS.TASK.SETFORM:
        return { ...state, inputtitle: action.payload.title, inputdate: action.payload.date, taskdatas: action.payload.taskdatas };
      case ACTYPS.TASK.RESETFORM:
        return { ...INITIAL_STATE, sums: state.sums };
      case ACTYPS.TASK.SORTABLELIST:
        return { ...state, sortablelist: action.payload };
      default:
        return state;
    }
};
