import $ from 'jquery';
//import ReactGA from 'react-ga';
import { Conf } from '../config';

/*ReactGA.initialize(Conf.gaid, {
  debug: false,
  titleCase: false
});*/

const Tracker = {
  getApptype() {
    const apptype = $('#AppContainer').attr('data-apptype');
    return apptype;
  },

  appAction(event) {
    /*ReactGA.event({
      category: 'App',
      action: event,
      label: this.getApptype()
    });*/
  },

  userAction(event) {
    /*ReactGA.event({
      category: 'User',
      action: event,
      label: this.getApptype()
    });*/
  },

  screenView(screen) {
    /*ReactGA.ga('send', 'screenview', {
      'appName': 'Multido',
      'appVersion': '1.0',
      'screenName': screen
    });*/
  },

};

export { Tracker };
