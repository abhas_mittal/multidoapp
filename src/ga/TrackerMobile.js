import { Platform } from 'react-native';
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
import { Conf } from '../config';

//const gaTracker = new GoogleAnalyticsTracker(Conf.gaid);

const Tracker = {
  getApptype() {
    const apptype = Platform.OS;
    return apptype;
  },

  appAction(event) {
    //gaTracker.trackEvent('App', event, { label: this.getApptype() });
  },

  userAction(event) {
    //gaTracker.trackEvent('User', event, { label: this.getApptype() });
  },

  screenView(screen) {
    //gaTracker.trackScreenView(screen);
  },

};

export default Tracker;
