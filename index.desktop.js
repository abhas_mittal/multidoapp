import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import Index from './src/index';

class App extends Component {
  render() {
    return (
      <Index platform="desktop" />
    );
  }
}

AppRegistry.registerComponent('Multido', () => App);
AppRegistry.runApplication('Multido', { rootTag: document.getElementById('AppContainer') });
