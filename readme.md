#Multiplatform todo application for all major platforms#
Javascript, React Native, Electron, Firebase

Multido is a cloud-based application family with the same codebase so it can be used on Mobile, Desktop or Web environment. Your data is hosted in the Firebase cloud. The system doesn’t use data encryption so you are advised not to store sensitive information on it.

**Article:** [https://medium.com/@attilaberki/yet-another-to-do-application-21509b30d79a](https://medium.com/@attilaberki/yet-another-to-do-application-21509b30d79a)

![hero.gif](https://bitbucket.org/repo/Bgg5RqG/images/229394916-hero.gif)



##Test with ready to use applications##

###iOS###
Available via TestFlight. Send an email for invitation: [testflight@multido.net](mailto:testflight@multido.net)

###Android###
Available via TestFairy:
[Download page](https://tsfr.io/GmgFr2)

###Desktop###
Packages in the downloads folder:
[Downloads Folder](https://bitbucket.org/attilaberki/multidoapp/downloads/)

###WebApp###
[app.multido.net](https://app.multido.net/)



##Install from repo##
Open a terminal on macOS (expected environment: react-native-cli, npm)

Open any directory

```
#!javascript

cd your/folder
```


**A.) One line command**

Copy this line to the terminal

```
#!javascript

git clone https://attilaberki@bitbucket.org/attilaberki/multidoapp.git && react-native init --version="0.43.2" Multido && rsync -a multidoapp/* Multido/ && rm -rf multidoapp && cd Multido
```

**B.) Detailed commands**

Clone the repository

```
#!javascript

git clone https://attilaberki@bitbucket.org/attilaberki/multidoapp.git
```

Initalize a React Native app as follows

```
#!javascript

react-native init --version="0.43.2" Multido
```

Copy the content of the repo then delete the repo

```
#!javascript

rsync -a multidoapp/* Multido/ && rm -rf multidoapp
```
Open the Multido directory and install dependencies

```
#!javascript

cd Multido
```

**A+B.) Initialize your firebase app**

Create a firebase application: [firebase.google.com](https://firebase.google.com/)

Open the Database menupoint in your firebase project and click the Rules tab. Paste this code then save.

```
#!javascript

{
  "rules": {
    "users": {
      "$uid": {
        ".read": "$uid === auth.uid",
        ".write": "$uid === auth.uid"
      }
    }
  }
}
```

**Enable** the **Email/Password** row in the **Authentication** / **Sign-in method** tab

Copy config data rows from the **Web setup** settings, and paste data to the **firebase** section of **src/config/Conf.js** file

Start the application from terminal
```
#!javascript

npm start
```

Desktop,- Web,- and Mobile application will open. After that might need to refresh the desktop app.





##Additional information##
About me:  [www.abg.hu](http://www.abg.hu/)