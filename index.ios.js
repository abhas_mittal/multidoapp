import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import Index from './src/index';

class App extends Component {
  render() {
    return (
      <Index platform="mobile" />
    );
  }
}

AppRegistry.registerComponent('Multido', () => App);
